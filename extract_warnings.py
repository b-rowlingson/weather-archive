import sys
from bs4 import BeautifulSoup
import re
import json
import urllib.request
from urllib.request import urlopen

URL = "https://www.metoffice.gov.uk/weather/warnings-and-advice/uk-warnings"

def getLatest(output_path):
    urllib.request.urlretrieve(URL, output_path)

def html_to_geojson(html):
    soup = BeautifulSoup(html, features="html.parser")
    script = soup.find("script")
    stext = script.text
    parts = stext.split(";")
    data = parts[2]
    data = re.sub(r"window.metoffice.*", "{\"data\":", data)
    data = re.sub(r"mapTilesUrl.*", "", data)
    data = re.sub(r"polygonsGeoJson:", "", data)
    data = re.sub(r"days:", "\"days\":", data)
    data = json.loads(data)
    data = data['data']
    return data

if __name__ == "__main__":

    output = urlopen(URL).read()
    html = output.decode('utf-8')
    data = html_to_geojson(html)
    print(json.dumps(data, indent=4))
    
    
    
