#!/bin/bash

# update warnings
URL="https://www.metoffice.gov.uk/weather/warnings-and-advice/uk-warnings"

## reset archive.zip if its in the repo. remove manually after CI run...
if [ -f archive.zip ] ; then
    mkdir -p public
    mv archive.zip public/
    mv latest.json public/
    exit 0
fi

echo get the latest zip 
wget --quiet --output-document archive.zip  https://b-rowlingson.gitlab.io/weather-archive/archive.zip || exit -77

## zero-length archive means 404d so STOP!
if [ ! -s archive.zip ] ; then
    rm archive.zip
    exit -99
fi

echo moving old archive.zip back to new public
mv archive.zip public/

echo get the last latest.json we wrote
wget --quiet --output-document latest.json  https://b-rowlingson.gitlab.io/weather-archive/latest.json 

echo put current latest.json into public
cp latest.json public/

echo Getting from $URL
# write to html since changes of nov 2023.
NOW=warns-`date -Iminutes`.html

echo get the latest warnings
# wget -O - $URL | grep geojson | sed 's/[^{]*//;s/;$//' > $NOW
# lets just archive the page and post-process later since things
# seem to have changed in november 2023...
## grep out some stuff that changes even if there's no warning changes
wget -O - $URL | grep -v menu-panel | grep -v error-banner | gzip -c > $NOW

echo compare with the current one on the web to the one we got now..
cmp -s latest.json $NOW

if [ $? = 0 ] ; then
    echo no change from latest.json to $NOW
    echo exiting with public...
    ls -l public/
    exit 0
fi


echo add $NOW
zip public/archive.zip $NOW

echo make the current into the new latest
# this isn't actually json any more since Nov 2023 changes...
mv $NOW public/latest.json

echo exiting with public...
ls -l public/




